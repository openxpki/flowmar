The CSR has been approved.
Current approvers are:
[% FOREACH key IN approvals.keys -%]
  [% key %] ([% approvals.$key %])
[% END %]
